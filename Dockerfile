FROM alpine:3.3


RUN \
    apk add --update python=2.7.11-r3 py-pip=7.1.2-r0 supervisor=3.1.3-r1 && rm -rf /var/cache/apk/* && \
    sed -i 's/^\(\[supervisord\]\)$/\1\nnodaemon=true/' /etc/supervisord.conf && \
    mkdir -p /var/log/supervisor/

EXPOSE 80

ENTRYPOINT ["supervisord", "--configuration", "/etc/supervisord.conf"]
